//
//  LocationCollectorApp.swift
//  LocationCollector
//
//  Created by YuanFang on 1/19/23.
//

import SwiftUI

@main
struct LocationCollectorApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
