//
//  LocationManager.swift
//  LocationCollector
//
//  Created by YuanFang on 1/19/23.
//

import Foundation
import SwiftUI
import CoreLocation
import MapKit

class LocationManager: NSObject, ObservableObject {

    private let locationManager: CLLocationManager

    @Published var authorizationStatus: CLAuthorizationStatus?
    @Published var location: CLLocation?

    override init() {
        locationManager = CLLocationManager()
        authorizationStatus = locationManager.authorizationStatus

        super.init()
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.distanceFilter = kCLDistanceFilterNone
        locationManager.startUpdatingLocation()
    }

    func permissionRequest() {
        locationManager.requestWhenInUseAuthorization()
    }

}

extension LocationManager: CLLocationManagerDelegate {

    func locationManagerDidChangeAuthorization(_ manager: CLLocationManager) {
        authorizationStatus = manager.authorizationStatus
    }

    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let location = locations.last else {
            return
        }
        self.location = location
    }

}
