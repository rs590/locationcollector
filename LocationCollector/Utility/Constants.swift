//
//  Constants.swift
//  LocationCollector
//
//  Created by Ruitong Su on 1/19/23.
//

import Foundation

enum Constants {
    static let baseURL = "http://rocco.colab.duke.edu/"
}

enum Endpoints {
    static let loactions = "locations"
}
