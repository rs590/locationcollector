//
//  Coordinator.swift
//  LocationCollector
//
//  Created by YuanFang on 1/20/23.
//

import Foundation
import MapKit
import Combine

final class MapViewCoordinator: NSObject, MKMapViewDelegate {
    var control: MapView

    var lastUserLocation: CLLocationCoordinate2D?
    var subscription: Set<AnyCancellable> = []

    var annotations = [LocationAnnotation]()

    init(control: MapView) {
        self.control = control
        super.init()

        // reset map center to user location
        NotificationCenter.default.publisher(for: .goToCurrentLocation)
            .receive(on: DispatchQueue.main)
            .sink { [weak self] _ in
                guard let lastUserLocation = self?.lastUserLocation else { return }
                control.map.setCenter(lastUserLocation, animated: true)
            }
            .store(in: &subscription)

        // add annotation
        NotificationCenter.default.publisher(for: .didAddLocation)
            .receive(on: DispatchQueue.main)
            .map { notification -> LocationAnnotation? in
                    notification.object as? LocationAnnotation
                }
            .sink(receiveValue: { locationAnnotation in
                guard let annotation = locationAnnotation else { return }
                self.annotations.append(annotation)
                control.map.addAnnotation(annotation)
            })
            .store(in: &subscription)

        // delete annotation
        NotificationCenter.default.publisher(for: .didDelteLocation)
            .receive(on: DispatchQueue.main)
            .map { notification -> LocationAnnotation? in
                    notification.object as? LocationAnnotation
                }
            .sink(receiveValue: { receivedAnnotation in
                guard let receivedAnnotation else { return }
                let foundedAnnotation = self.annotations.first {
                    $0.location.coordinate.longitude == receivedAnnotation.location.coordinate.longitude &&
                    $0.location.coordinate.latitude == receivedAnnotation.location.coordinate.latitude
                }
                if let foundedAnnotation {
                    control.map.removeAnnotation(foundedAnnotation)
                }
            })
            .store(in: &subscription)

        // Update annotations
        NotificationCenter.default.publisher(for: .didFetchLocation)
            .receive(on: DispatchQueue.main)
            .map { notification -> [LocationAnnotation]? in
                    notification.object as? [LocationAnnotation]
                }
            .sink(receiveValue: { receivedAnnotations in
                guard let receivedAnnotations else { return }
                // remove old annotations
                self.annotations.forEach {
                    control.map.removeAnnotation($0)
                }
                self.annotations.removeAll()
                // add received annotations
                receivedAnnotations.forEach {
                    self.annotations.append($0)
                    control.map.addAnnotation($0)
                }
            })
            .store(in: &subscription)
    }

    func mapView(_ mapView: MKMapView, didUpdate userLocation: MKUserLocation) {
        lastUserLocation = userLocation.coordinate
    }

    func mapView(_ mapView: MKMapView, didAdd views: [MKAnnotationView]) {

        if let annotationView = views.first {
            if let annotation = annotationView.annotation {
                if annotation is MKUserLocation {

                    let region = MKCoordinateRegion(center: annotation.coordinate,
                                                    latitudinalMeters: 1000,
                                                    longitudinalMeters: 1000)
                    mapView.setRegion(region, animated: true)
                }
            }
        }
    }

    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        guard annotation is MKPointAnnotation else { return nil }

            let identifier = "Annotation"
            var annotationView = mapView.dequeueReusableAnnotationView(withIdentifier: identifier)

            if annotationView == nil {
                annotationView = MKMarkerAnnotationView(annotation: annotation, reuseIdentifier: identifier)
                annotationView!.canShowCallout = true
            } else {
                annotationView!.annotation = annotation
            }

            return annotationView
    }
}
