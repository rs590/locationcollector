//
//  Notification+.swift
//  LocationCollector
//
//  Created by YuanFang on 1/22/23.
//

import Foundation

extension Notification.Name {
    static let goToCurrentLocation = Notification.Name("goToCurrentLocation")
    static let didAddLocation = Notification.Name("didAddLocation")
    static let didFetchLocation = Notification.Name("didFetchLocation")
    static let didDelteLocation = Notification.Name("didDeleteLocation")
}
