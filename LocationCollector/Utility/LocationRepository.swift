//
//  LocationRepository.swift
//  LocationCollector
//
//  Created by Ruitong Su on 1/19/23.
//

import Foundation
import MapKit

final class LocationRepository: ObservableObject {
    @Published var locations = [Location]()

    func fetchLocations() async throws {
        let urlString =  Constants.baseURL + Endpoints.loactions

        guard let url = URL(string: urlString) else {
            throw HttpError.badURL
        }

        let locationResponse: [Location] = try await NetworkManager.shared.fetch(url: url)

        DispatchQueue.main.async {
            self.locations = locationResponse
        }

        // post new annotations to map view
        var annotations = [LocationAnnotation]()
        locations.forEach { location in
            let locationData = CLLocation(latitude: Double(location.latitude) ?? 0,
                                      longitude: Double(location.longitude) ?? 0)
            let locationAnnotation = LocationAnnotation(label: location.label, location: locationData)
            annotations.append(locationAnnotation)
        }
        NotificationCenter.default.post(name: .didFetchLocation, object: annotations)
    }

    func delete(at offsets: IndexSet) {
        offsets.forEach { idx in
            guard let id = locations[idx].id,
                  let url = URL(string: Constants.baseURL + Endpoints.loactions + "/\(id)") else {
                return
            }

            Task {
                do {
                    try await NetworkManager.shared.delete(at: id, url: url)
                } catch {
                    print("❌ [Error] [Delete] \(error)")
                }
            }

            // post notification to delete annotation on map
            let location = CLLocation(latitude: Double(locations[idx].latitude) ?? 0,
                                      longitude: Double(locations[idx].longitude) ?? 0)
            let locationAnnotation = LocationAnnotation(label: locations[idx].label, location: location)
            NotificationCenter.default.post(name: .didDelteLocation, object: locationAnnotation)
        }

        locations.remove(atOffsets: offsets)
    }
}
