//
//  TrackingViewModel.swift
//  LocationCollector
//
//  Created by Ruitong Su on 1/22/23.
//

import Foundation
import MapKit

final class TrackingViewModel: ObservableObject {
    /// LABEL
    @Published var label: String = ""
    /// DATA
    @Published var latitude: String = ""
    @Published var longitude: String = ""
    @Published var altitude: String = ""

    func addLocation() async throws {
        let urlString = Constants.baseURL + Endpoints.loactions
        guard let url = URL(string: urlString) else {
            throw HttpError.badURL
        }

        let location = Location(id: nil,
                                createdAt: nil,
                                updatedAt: nil,
                                label: label,
                                latitude: latitude,
                                longitude: longitude,
                                altitude: altitude)

        try await NetworkManager.shared.send(to: url,
                                             object: location,
                                             httpMethod: HttpMethods.POST.rawValue)
        // Drop a pin at Map View
        NotificationCenter.default.post(name: .didAddLocation,
                                        object: LocationAnnotation(label: label,
                                                                   location: CLLocation(latitude: Double(latitude) ?? 0,
                                                                                        longitude: Double(longitude) ?? 0)))
    }
}
