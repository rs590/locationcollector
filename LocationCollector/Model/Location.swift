//
//  Location.swift
//  LocationCollector
//
//  Created by Ruitong Su on 1/19/23.
//

import Foundation

struct Location: Identifiable, Codable {
    let id: UUID?
    let createdAt: String?
    let updatedAt: String?
    /// Description of the location
    let label: String
    /// Geography Data
    let latitude: String
    let longitude: String
    let altitude: String
}
