//
//  MapView.swift
//  LocationCollector
//
//  Created by YuanFang on 1/19/23.
//

import SwiftUI
import MapKit

struct MapView: UIViewRepresentable {
    @EnvironmentObject var repo: LocationRepository

    let map = MKMapView()

    private var annotations: [LocationAnnotation] {
        var annotations = [LocationAnnotation]()
        repo.locations.forEach { location in
            annotations.append(
                LocationAnnotation(label: location.label,
                                   location: CLLocation(latitude: Double(location.latitude) ?? 0,
                                                        longitude: Double(location.longitude) ?? 0))
            )
        }
        return annotations
    }

    func makeUIView(context: Context) -> MKMapView {
        map.showsUserLocation = true
        map.delegate = context.coordinator
        return map
    }

    func makeCoordinator() -> MapViewCoordinator {
        MapViewCoordinator(control: self)
    }

    func updateUIView(_ uiView: MKMapView, context: UIViewRepresentableContext<MapView>) { }
}
