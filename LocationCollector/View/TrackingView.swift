//
//  TrackingView.swift
//  LocationCollector
//
//  Created by Ruitong Su on 1/21/23.
//

import SwiftUI
import CoreLocation
import MapKit

struct TrackingView: View {
    @EnvironmentObject var locationManager: LocationManager
    @EnvironmentObject var repo: LocationRepository
    @StateObject var viewModel = TrackingViewModel()
    @State private var showAlert = false

    var coordinate: CLLocationCoordinate2D? {
        locationManager.location?.coordinate
    }

    var latitudeStr: String {
        String(coordinate?.latitude ?? 0)
    }

    var longitudeStr: String {
        String(coordinate?.longitude ?? 0)
    }

    var altitudeStr: String {
        String(locationManager.location?.altitude ?? 0)
    }

    var body: some View {
        VStack {
            /// MAP VIEW
            MapView()
                .environmentObject(repo)
                .padding()

            /// SAVE LOCATION DATA
            HStack {
                // Save Button
                SaveButton(showAlert: $showAlert, enteredText: $viewModel.label) {
                    // passing data to viewModel
                    viewModel.latitude = latitudeStr
                    viewModel.longitude = longitudeStr
                    viewModel.altitude = altitudeStr
                    // let viewModel add the location
                    // let mapView add a pin
                    Task {
                        do {
                            try await viewModel.addLocation()
                            // clear label text
                            viewModel.label = ""
                        } catch {
                            print("❌ [Error] [Add Location] \(error)")
                        }
                    }
                }
                .padding()

                Spacer()

                // Reset Map Center to current user location button
                Button {
                    goToUserLocation()
                } label: {
                    Image(systemName: "location.fill")
                        .font(.title2)
                        .padding(10)
                        .clipShape(Circle())
                }
                .frame(maxWidth: .infinity, alignment: .trailing)
                .padding()
            }

            /// LOCATION DATA
            VStack {
                PairView(
                    leftText: "Latitude:",
                    rightText: latitudeStr
                )
                PairView(
                    leftText: "Longitude:",
                    rightText: longitudeStr
                )
                PairView(
                    leftText: "Altitude:",
                    rightText: altitudeStr
                )
            }
            .padding()
        }
        .onAppear {
            Task {
                do {
                    try await repo.fetchLocations()
                } catch {
                    print("❌ [Error] [Fetch] \(error)")
                }
            }
        }
    }

    private func goToUserLocation() {
        NotificationCenter.default.post(name: .goToCurrentLocation, object: nil)
    }
}

struct PairView: View {
    let leftText: String
    let rightText: String

    var body: some View {
        HStack {
            Text(leftText)
            Spacer()
            Text(rightText)
        }
    }
}

struct SaveButton: View {
    @Binding var showAlert: Bool
    @Binding var enteredText: String

    let completion: () -> Void

    var body: some View {
        Button("Save Location") {
            self.showAlert = true
        }
        .alert("Add Label", isPresented: $showAlert, actions: {
            TextField("Label", text: $enteredText)
            Button("OK", action: {
                self.showAlert = false
                completion()
            })
            Button("Cancel", role: .cancel, action: {
                enteredText = ""
                self.showAlert = false
            })
        }, message: {
            Text("Please enter a label for current location.")
        })
    }
}

struct TrackingView_Previews: PreviewProvider {
    static var previews: some View {
        TrackingView()
    }
}
