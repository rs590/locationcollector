//
//  LocationListView.swift
//  LocationCollector
//
//  Created by Ruitong Su on 1/19/23.
//

import SwiftUI

struct LocationListView: View {
    @EnvironmentObject var repo: LocationRepository

    var body: some View {
        NavigationView {
            List {
                ForEach(repo.locations) { location in
                    LocationListCell(label: location.label,
                                     latitude: location.latitude,
                                     longitude: location.longitude,
                                     altitude: location.altitude)
                }
                .onDelete(perform: repo.delete)
            }
            .navigationTitle("📖Location List")
            .onAppear {
                Task {
                    do {
                        try await repo.fetchLocations()
                    } catch {
                        print("❌ [Error] [Fetch] \(error)")
                    }
                }
            }
        }
    }
}

struct LocationListView_Previews: PreviewProvider {
    static var previews: some View {
        LocationListView()
    }
}
