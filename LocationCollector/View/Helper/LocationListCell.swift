//
//  LocationListCell.swift
//  LocationCollector
//
//  Created by Ruitong Su on 1/24/23.
//

import SwiftUI

struct LocationListCell: View {
    @State var label: String
    @State var latitude: String
    @State var longitude: String
    @State var altitude: String

    var body: some View {
        VStack(alignment: .leading) {
            Text(label)
            HStack {
                Text("La: \(latitude)")
                Text("Lo: \(longitude)")
                Text("Al: \(altitude)")
            }
            .font(.subheadline).foregroundColor(.gray)
        }
    }
}
