//
//  LocationAnnotation.swift
//  LocationCollector
//
//  Created by Ruitong Su on 1/22/23.
//

import Foundation
import MapKit

final class LocationAnnotation: NSObject, Identifiable {
    let id = UUID()
    let label: String
    let location: CLLocation

    init(label: String, location: CLLocation) {
        self.label = label
        self.location = location
    }
}

extension LocationAnnotation: MKAnnotation {
    var coordinate: CLLocationCoordinate2D {
        location.coordinate
    }

    var title: String? {
        label
    }
}
