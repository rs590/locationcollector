//
//  ContentView.swift
//  LocationCollector
//
//  Created by YuanFang on 1/19/23.
//

import SwiftUI
import CoreLocation
import MapKit

struct ContentView: View {

    @StateObject var locationManager = LocationManager()
    @StateObject var locationRepo = LocationRepository()

    var body: some View {

        VStack {
            switch locationManager.authorizationStatus {
            case .notDetermined:
                AnyView(RequestLocationView())
                    .environmentObject(locationManager)
            case .restricted:
                ErrorView(errorMsg: "Location use is restricted.")
            case .denied:
                ErrorView(errorMsg: "Permission denied.")
            case .authorizedAlways, .authorizedWhenInUse:
                TabView {
                    TrackingView()
                        .environmentObject(locationManager)
                        .environmentObject(locationRepo)
                        .tabItem {
                            Image(systemName: "location.circle")
                            Text("Tracking")
                        }

                    LocationListView()
                        .environmentObject(locationRepo)
                        .tabItem {
                            Image(systemName: "list.bullet.circle")
                            Text("List")
                        }
                }
            default:
                Text("Unexpected status.")
            }
        }
    }
}

struct RequestLocationView: View {
    @EnvironmentObject var locationManager: LocationManager

    var body: some View {
        VStack {
            Image(systemName: "location.circle")
                .resizable()
                .frame(width: 100, height: 100, alignment: .center)
                .foregroundColor(.blue)
            Button(action: {
                locationManager.permissionRequest()
            }, label: {
                Label("Allow tracking", systemImage: "location")
            })
            .padding(10)
            .foregroundColor(.white)
            .background(Color.blue)
            .clipShape(RoundedRectangle(cornerRadius: 8))
            Text("We need your permission to get your location.")
                .foregroundColor(.gray)
                .font(.caption)
        }
    }
}

struct ErrorView: View {
    var errorMsg: String

    var body: some View {
        VStack {
            Image(systemName: "xmark.octagon")
                .resizable()
                .frame(width: 100, height: 100, alignment: .center)
            Text(errorMsg)
        }
        .padding()
        .foregroundColor(.white)
        .background(Color.red)
        .clipShape(RoundedRectangle(cornerRadius: 12))
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
